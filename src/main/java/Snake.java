import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.input.KeyCode;
import javafx.scene.paint.Color;

import java.util.ArrayList;
import java.util.Collections;

public class Snake extends Node {
    public boolean isAlive = true;
    private Color headColor = Color.rgb(0,0,0);
    private double headX = 30; //new Rnd().generateRandomNumber();
    private double headY = 30; //new Rnd().generateRandomNumber();
    private Direction headDirection = Direction.LEFT;
    private Direction tailDirection = Direction.RIGHT;
    public ArrayList<Pixel> body = new ArrayList<Pixel>();
    private ArrayList<Integer> bodyIdArray = new ArrayList<Integer>();
    private int bodySize;
    private Pixel head;
    private Pixel tail;

    public Snake() {
        this.initBody(3);
        head = body.get(0);
        tail = body.get(body.size()-1);
    }

    public void initBody(int bodyLength) {
        body.add(new Pixel(headX, headY, headColor));
        Double headIdDouble = body.get(0).getPixelId();
        getBodyIdArray().add(headIdDouble.intValue());
        for (int i=1; i<=bodyLength; i++) {
            body.add(new Pixel(body.get(i-1).getX() + 10, body.get(i-1).getY(), Color.rgb(i*5, i*5, i*5)));
            Double idDouble = body.get(i).getPixelId();
            getBodyIdArray().add(idDouble.intValue());
        }
    }

    public void addBodyPart() {
        int tailIndex = body.size() - 1;
        if (tailIndex <= 255/5-1) {
            body.add(new Pixel(body.get(tailIndex).getX(), body.get(tailIndex).getY(), Color.rgb((tailIndex+1) * 5, (tailIndex+1) * 5, (tailIndex+1) * 5)));
        } else {
            body.add(new Pixel(body.get(tailIndex).getX(), body.get(tailIndex).getY(), Color.rgb(255, 255, 255)));
        }
        getActualTail();
        getActualBodySize();
        Double idDouble = body.get(tailIndex).getPixelId();
        getBodyIdArray().add(idDouble.intValue());
    }

    private void inverseBody() {
        double reverseHeadX = getActualTail().getX();
        double reverseHeadY = getActualTail().getY();
        Collections.reverse(body);
        head = body.get(0);
        calcActualHeadCoord(reverseHeadX, reverseHeadY);
    }

    private Pixel getActualTail() {
        int tailIndex = body.size() - 1;
        return tail = body.get(tailIndex);
    }

    private Direction getTailDirection() {
        int tailIndex = body.size() - 1;
        int nextAfterTailIndex = tailIndex - 1;
        if (body.get(nextAfterTailIndex).getY() == body.get(tailIndex).getY()) {
            if (body.get(nextAfterTailIndex).getX() >= body.get(tailIndex).getX()) {
                return tailDirection = Direction.LEFT;
            } else {
                return tailDirection = Direction.RIGHT;
            }
        }
        if (body.get(nextAfterTailIndex).getX() == body.get(tailIndex).getX()) {
            if (body.get(nextAfterTailIndex).getY() >= body.get(tailIndex).getY()) {
                return tailDirection = Direction.UP;
            } else {
                return tailDirection = Direction.DOWN;
            }
        }
        return tailDirection;
    }


    public void moveSnake(SnakeGame snakeGame) {
        Scene scene = snakeGame.getScene();
        scene.setOnKeyPressed(keyEvent -> {
            KeyCode keyCode = keyEvent.getCode();
            switch (keyCode) {
                case RIGHT:
                    if (headDirection == Direction.LEFT) {
                        if (headDirection == getTailDirection()) {
                            inverseBody();
                            moveTo(headDirection);
                            break;
                        }
                        inverseBody();
                    }
                    headDirection = Direction.RIGHT;
                    moveTo(headDirection);
                    break;
                case LEFT:
                    if (headDirection == Direction.RIGHT) {
                        if (headDirection == getTailDirection()) {
                            inverseBody();
                            moveTo(headDirection);
                            break;
                        }
                        inverseBody();
                    }
                    headDirection = Direction.LEFT;
                    moveTo(headDirection);
                    break;
                case UP:
                    if (headDirection == Direction.DOWN) {
                        if (headDirection == getTailDirection()) {
                            inverseBody();
                            moveTo(headDirection);
                            break;
                        }
                        inverseBody();
                    }
                    headDirection = Direction.UP;
                    moveTo(headDirection);
                    break;
                case DOWN:
                    if (headDirection == Direction.UP) {
                        if (headDirection == getTailDirection()) {
                            inverseBody();
                            moveTo(headDirection);
                            break;
                        }
                        inverseBody();
                    }
                    headDirection = Direction.DOWN;
                    moveTo(headDirection);
                    break;
            }
            snakeGame.gameLogic();
        });
    }

    private void moveTo (Direction direction) {
        Double newX;
        Double newY;
        switch (direction) {
            case RIGHT:
                newX = getHead().getX() + 10;
                newY = getHead().getY();
                break;
            case LEFT:
                newX = getHead().getX() - 10;
                newY = getHead().getY();
                break;
            case UP:
                newX = getHead().getX();
                newY = getHead().getY() - 10;
                break;
            case DOWN:
                newX = getHead().getX();
                newY = getHead().getY() + 10;
                break;
            default:
                newX = getHead().getX();
                newY = getHead().getY();
                new IllegalArgumentException();
        }
        calcActualCoord(newX, newY);
    }

    private void calcActualCoord(double newX, double newY){
        for (int i=body.size()-1; i>0; i--) {
            double prevBodyPartX = body.get(i-1).getX();
            double prevBodyPartY = body.get(i-1).getY();
            body.get(i).setX(prevBodyPartX);
            body.get(i).setY(prevBodyPartY);
            body.get(i).setPixelId(head.coordToId(prevBodyPartY, prevBodyPartX));
        }
        calcActualHeadCoord(newX, newY);
    }

    private void calcActualHeadCoord(double newX, double newY) {
        head.setX(newX);
        head.setY(newY);
        head.setPixelId(head.coordToId(newY, newX));
    }

    public Pixel getHead() {
        return head;
    }

    public int getActualBodySize() {
        return bodySize = body.size();
    }

    public void setSnakeColor(Color headColor) {
        this.headColor = headColor;
        for (Pixel bodyPart : body) {
            bodyPart.setFill(this.headColor);
        }
        this.getHead().setFill(this.headColor);
    }

    public ArrayList<Integer> getBodyIdArray() {
        return bodyIdArray;
    }

    public ArrayList<Integer> getSortedBodyIdArray() {
        ArrayList<Integer> sortedArray = new ArrayList<>(bodyIdArray);
        Collections.sort(sortedArray, Collections.reverseOrder());
        return sortedArray;
    }

    public boolean checkSelfCollision() {
        boolean isSelfCollision = body.stream().skip(1).anyMatch(bodyPart -> head.getPixelId() == bodyPart.getPixelId());
        return isSelfCollision;
    }
}
