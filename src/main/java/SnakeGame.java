import javafx.application.Application;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.paint.Color;
import javafx.scene.text.Text;
import javafx.stage.Stage;

public class SnakeGame extends Application{

    public static final int FIELD_SIZE = 32;
    private Group field;
    Pixel[][] fieldPixels = new Pixel[30][30];
    private Scene scene;
    private Wall wall;
    private Snake snake;
    private Apple apple;
    private Text text;
    private int score;

    public static void main(String[] args) {
        Application.launch(args);
    }

    @Override
    public void init() {
        field = new Group();
        wall = new Wall();
        snake = new Snake();
        apple = new Apple(snake.getSortedBodyIdArray());
        text = new Text("Очки: " + score);
    }

    @Override
    public void start(Stage stage) {

        for (int i=1; i<=30; i++) {
            for (int j=1; j<=30; j++) {
                fieldPixels[i-1][j-1] = new Pixel(i * 10, j * 10, Color.DARKSEAGREEN);
                field.getChildren().addAll(fieldPixels[i-1][j-1]);
            }
        }
        field.getChildren().add(wall.createWallBox());

        text.setFill(Color.WHITE);
        text.setLayoutY(326);
        text.setLayoutX(150);

        field.getChildren().add(text);

        field.getChildren().add(apple);

        for (Pixel snakeBody : snake.body) {
            field.getChildren().add(snakeBody);
        }

        scene = new Scene(field, FIELD_SIZE * 10, 370, Color.BLACK);

        stage.setScene(getScene());
        stage.setTitle("SNAKE");
        stage.setWidth(FIELD_SIZE * 10);
        stage.setHeight(360);
        stage.show();
        snake.moveSnake(this);
    }

    public void gameLogic() {
        int lastChildIndex = field.getChildren().size();
        // столкновение со стеной
        if (snake.getHead().getX() == ((Pixel) wall.getWallGroup().getChildren().get(0)).getX() ||
                snake.getHead().getY() == ((Pixel) wall.getWallGroup().getChildren().get(0)).getY() ||
                snake.getHead().getX() == ((Pixel) wall.getWallGroup().getChildren().get(63)).getX() ||
                snake.getHead().getY() == ((Pixel) wall.getWallGroup().getChildren().get(63)).getY())
        {
                snake.setSnakeColor(Color.RED);
        }
        if (snake.checkSelfCollision()){
            snake.setSnakeColor(Color.RED);
        }


        // Поедание яблока
        if (snake.getHead().getX() == apple.getX()) {
            if (snake.getHead().getY() == apple.getY()) {
                appleEaten();
                snake.addBodyPart();
                field.getChildren().add(lastChildIndex-1, snake.body.get(snake.body.size() - 1));
                score = Apple.getCount() - 1;
                text.setText("Очки: " + score);
            }
        }
//        for (int i = 0; i<5000; i++) {
//            field.getChildren().add(new Apple(snake.getSortedBodyIdArray()));
//        }
    }

    private void appleEaten() {
        field.getChildren().remove(apple);
        apple = new Apple(snake.getSortedBodyIdArray());
        field.getChildren().add(apple);
    }

    public Scene getScene() {
        return scene;
    }
}
