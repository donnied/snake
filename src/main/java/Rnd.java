import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.ThreadLocalRandom;

public class Rnd {

    private static final int rangeMin = 32;
    private static final int rangeMax = 992;
    private static final int rangeMinCoord = 1;
    private static final int rangeMaxCoord = 32;
    List<Integer> shuffledFieldIds = new ArrayList<>();

    public int generateRandomNumber() {
        int randomValue = ThreadLocalRandom.current().nextInt(rangeMinCoord, rangeMaxCoord + 1) * 10;
        return randomValue;
    }

    /** Генерируем случайное число в диапазоне от и до,
     *  но не включая числа из упорядоченного массива exclude */
    public int getRandomWithExclusion(Integer... exclude) {
        // Если все кнопки на поле уже нажаты возвращаем -1
        if (exclude.length >= (rangeMax - rangeMin + 1)) {
            System.out.println("ИИ больше некуда ходить!");
            return -1;
        }
        // Если еще есть куда ходить
        else {
            int random = rangeMin + ThreadLocalRandom.current().nextInt((rangeMax - rangeMin + 1) - exclude.length);
            for (int ex : exclude) {
                if (random < ex) {
                    break;
                }
                random++;
            }
            return random;
        }
    }

    public Integer createArrayOfFieldIds(ArrayList<Integer> excludeArray) {
        for (int i=0; i<=rangeMax; i++) {
            shuffledFieldIds.add(i);
        }
        // минус змейка
        for (int i : excludeArray) {
            shuffledFieldIds.remove(i);
        }
        // левая стена
        for (int i=rangeMin+1; i<=rangeMax; i+=32) {
            shuffledFieldIds.remove(Integer.valueOf(i));
        }
        // правая стена
        for (int i=rangeMin*2; i<=rangeMax; i+=32) {
            shuffledFieldIds.remove(Integer.valueOf(i));
        }
        // верхняя стена
        for (int i=0; i<=rangeMin; i++) {
            shuffledFieldIds.remove(Integer.valueOf(i));
        }

        Collections.shuffle(shuffledFieldIds);
        return shuffledFieldIds.get(0);
    }
}
