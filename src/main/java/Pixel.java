import javafx.event.EventHandler;
import javafx.scene.input.MouseEvent;
import javafx.scene.paint.Color;
import javafx.scene.shape.Rectangle;


public class Pixel extends Rectangle {

    public static final double PIXEL_SIZE = 10;
    public static final double WIDTH = PIXEL_SIZE;
    public static final double HEIGHT = PIXEL_SIZE;
    private double pixelId;
    protected static Color color = Color.WHITE;
    private boolean isCollision = false;

    public Pixel() {
        this.setWidth(WIDTH);
        this.setHeight(HEIGHT);
        this.setEventListener();
    }

    public Pixel(double x, double y, Color color) {
        this();
        this.setX(x);
        this.setY(y);
        this.setPixelId(coordToId(y, x));
        this.setFill(color);
    }

    public Pixel(int id, Color color) {
        this();
        this.setX(idToColumnCoord(id));
        this.setY(idToRowCoord(id));
        this.setPixelId(id);
        this.setFill(color);
    }

    /** @return возвращает id кнопки, получая на вход координаты кнопки и размер стороны поля */
    public double coordToId(double rowCoord, double columnCoord) {
        double pixelId = (SnakeGame.FIELD_SIZE * rowCoord/10) + columnCoord/10 + 1;
        return pixelId;
    }

    /** @return возвращает координату строки, получая на вход id кнопки и размер стороны поля */
    static public int idToRowCoord(int buttonId) {
        int rowCoord = (buttonId - 1) / SnakeGame.FIELD_SIZE * 10;
        return rowCoord;
    }

    /** @return возвращает координату столбца, получая на вход id кнопки и размер стороны поля */
    static public int idToColumnCoord(int buttonId) {
        int columnCoord = (buttonId - 1) % SnakeGame.FIELD_SIZE * 10;
        return columnCoord;
    }

    public void setEventListener() {
        this.setOnMouseClicked(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                Pixel pixel = (Pixel) event.getSource();
                pixel.setFill(Color.BLUEVIOLET);
            }
        });
        this.setOnMousePressed(e ->
                {
                    this.setFill(Color.GREENYELLOW);
                });
    }

    public double getPixelId() {
        return pixelId;
    }

    public void setPixelId(double pixelId) {
        this.pixelId = pixelId;
    }

    public boolean isCollision() {
        return isCollision;
    }

    public void setCollision(boolean collision) {
        isCollision = collision;
    }
}
