import javafx.scene.paint.Color;

import java.util.ArrayList;

public class Apple extends Pixel {

    private static int count = 0;
    private int id;

    public Apple(ArrayList<Integer> excludeArray) {
        color = Color.BLUE;
        id = new Rnd().createArrayOfFieldIds(excludeArray);
        this.setX(idToColumnCoord(id));
        this.setY(idToRowCoord(id));
        setFill(color);
        count = getCount() + 1;
    }

    public static int getCount() {
        return count;
    }

    private int appleRandomId(Integer... excludeArray) {
        return new Rnd().getRandomWithExclusion(excludeArray);
    }
}
