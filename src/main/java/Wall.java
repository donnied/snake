import javafx.scene.Group;
import javafx.scene.paint.Color;

public class Wall extends Pixel {

    private static final Color color = Color.rgb(33, 120, 22);
    private Pixel[][] wallPixelsHorizontal = new Pixel[32][4];
    private Pixel[][] wallPixelsVertical = new Pixel[4][32];
    private Group wallGroup;

    public Wall() {
        super(0, 0, color);
        setCollision(true);
    }

    public Group createWallBox(){
        wallGroup = new Group();
        createHorizontalWalls(getWallGroup());
        createVerticalWalls(getWallGroup());
        return getWallGroup();
    }

    private Group createHorizontalWalls(Group wallGroup) {
        for (int i = 0; i <= 31; i++) {
            for (int j = 0; j <= 1; j++) {
                wallPixelsHorizontal[i][j] = new Pixel(i * 10, j * 10 * 31, color);
                wallGroup.getChildren().add(wallPixelsHorizontal[i][j]);
            }
        }
        return wallGroup;
    }

    private Group createVerticalWalls(Group wallGroup) {
        for (int i = 0; i <= 1; i++) {
            for (int j = 0; j <= 31; j++) {
                wallPixelsVertical[i][j] = new Pixel(i * 10 * 31, j * 10, color);
                wallGroup.getChildren().add(wallPixelsVertical[i][j]);
            }
        }
        return wallGroup;
    }

    public Group getWallGroup() {
        return wallGroup;
    }
}